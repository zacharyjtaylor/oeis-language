import urllib
def rs(n):
    try:
        return open("b"+n+".txt").read()
    except:
        return urllib.urlopen("https://oeis.org/A"+n+"/b"+n+".txt").read()

fit_six = lambda q:"0"*(6-len(q))+q
oeis = lambda n,v:int({a:b for a,b in filter(lambda x:x!=[],[[int(j)for j in i.split(" ")if j and not("#"in i)]for i in rs(fit_six(str(n))).split("\n")])}[v])
def main(S,n):
 seqs=map(''.join,zip(S[::6],S[1::6],S[2::6],S[3::6],S[4::6],S[5::6]))
 v = n
 for i in seqs[::-1]:
  v = oeis(int(i),v)
 return v